﻿using System.Collections;
using System.Reflection;
using System.Xml.Linq;

namespace Converter
{
    public class CSV
    {
        private char _delimiter;
        public CSV(char delimiter = ';')
        {
            _delimiter = delimiter;
        }
        public async Task Serialize(Stream stream, object data)
        {
            using (var sw = new StreamWriter(stream))
            {
                foreach (var item in (IList)data)
                {
                    await sw.WriteLineAsync(GetString(item));
                }
            }
        }
        private IEnumerable<FieldInfo> GetFields(object item) => ((TypeInfo)item.GetType()).DeclaredFields;
        private string GetString(object item) => string.Join(_delimiter, GetFields(item).Select(s => CheckValue(s.GetValue(item))));
        private string CheckValue(object? value)
        {
            if (value.ToString().Contains(";")) return $"\"{value}\"";
            return value.ToString();
        }



        public async Task<List<T>> Deserialize<T>(Stream stream) where T : new()
        {
            List<T> result = new List<T>();
            Type type = typeof(T);
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    result.Add((T)GetValue(type, await reader.ReadLineAsync() ?? ""));
                }
            }
            return result;
        }
        private object GetValue(Type type, string text)
        {
            var _object = Activator.CreateInstance(type);
            string[] arr = GetListValuesString(text);
            int index = 0;
            foreach (FieldInfo field in GetFields(_object))
            {
                if (index < arr.Length)
                {
                    field.SetValue(_object, Convert.ChangeType(arr[index], field.FieldType));
                    index++;
                }
            }
            return _object;
        }

        private string[] GetListValuesString(string text)
        {
            List<string> arr = new List<string>();
            string elem = string.Empty;
            bool openElem = false;
            bool openElemLimit = false;
            foreach (var sym in text)
            {
                if (!openElem)
                {
                    if (sym == '"')
                    {
                        openElem = true;
                        openElemLimit = true;
                        continue;
                    }
                    openElem = true;
                }
                else if (openElem)
                {
                    if (openElemLimit)
                    {
                        if (sym == '"')
                        {
                            openElemLimit = false;
                            continue;
                        }
                    }
                    else if (sym == _delimiter)
                    {
                        openElem = false;
                        arr.Add(elem);
                        elem = string.Empty;
                        continue;
                    }
                }
                elem += sym;
            }
            if (elem != string.Empty)
                arr.Add(elem);

            return arr.ToArray();
        }
    }
}
