﻿using CustomerRepository;
using Models;

namespace DataLoader
{
    public class DataLoader : IDataLoader
    {
        ///Вот хрень то
        public async Task LoadData(List<Customer> customers, int countThread)
        {
            int stepSkip = customers.Count() / countThread;
            //https://professorweb.ru/my/csharp/thread_and_files/1/1_12.php
            //https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.countdownevent?view=net-7.0
            //Есил не знаешь об этом классе, то и хрен догадаешься что такой есть. Мне подсказал мой гуру....
            using (CountdownEvent countEvent = new CountdownEvent(countThread))
            {
                for (int i = 0; i < countThread; i++)
                {
                    List<Customer> customersList = customers.Skip(i * stepSkip).Take(stepSkip).ToList();
                    int index = i;
                    ThreadPool.QueueUserWorkItem(
                        new WaitCallback(s => 
                            { 
                                LoadCustomerThread(customersList, index).Wait();
                                //Вот тут мы говорим что отпахал
                                countEvent.Signal();
                            }));
                }
                countEvent.Wait();
            }
        }
        private async Task LoadCustomerThread(List<Customer> customers, int index)
        {
            Thread thread = Thread.CurrentThread;
            using (var db = new DataBaseContext())
            {
                var repository = CustomerRepositoryFactory.GetReposiotory(db);
                bool isTry = false;
                int count = 1;
                while (!isTry)
                {
                    isTry = await repository.AddCustomers(customers);
                    if (!isTry)
                    {
                        count++;
                        Console.WriteLine($"Поток {index} ({thread.ManagedThreadId}) загрузка неуспешна. Пробуем ещё раз.");
                        Thread.Sleep(2000);
                    }
                }
                Console.WriteLine($"Поток {index} ({thread.ManagedThreadId}) загрузка успешна (попыток {count})");
                return;
            }
        }
    }
}
