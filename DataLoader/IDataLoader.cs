﻿using Models;

namespace DataLoader
{
    public interface IDataLoader
    {
        Task LoadData(List<Customer> customers, int countThread);
    }
}
