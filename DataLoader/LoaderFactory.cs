﻿namespace DataLoader
{
    public static class LoaderFactory
    {
        public static IDataLoader GetLoader()
        {
            return new DataLoader();
        }
    }
}
