﻿using DataParser.Component;
using Models;

namespace DataParser
{
    public static class ParserFactory
    {
        public static IDataParser GetGenerator(DataType typeGenerate, string pathFile)
        {
            switch (typeGenerate)
            {
                case DataType.XML: return new XmlParser(pathFile);
                case DataType.JSON: return new JsonParser(pathFile);
                case DataType.CSV: return new CsvParser(pathFile);
                default: return new XmlParser(pathFile);
            }
        }
    }
}
