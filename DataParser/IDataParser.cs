﻿using Models;

namespace DataParser
{
    public interface IDataParser
    {
        Task<List<Customer>> Parse();
    }
}
