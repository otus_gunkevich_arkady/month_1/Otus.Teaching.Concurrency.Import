﻿using Models;

namespace DataParser.Component
{
    public class CsvParser : IDataParser
    {
        private readonly string _path;
        public CsvParser(string path)
        {
            _path = path;
        }
        public async Task<List<Customer>> Parse()
        {
            using (var stream = File.Open(_path, FileMode.Open, FileAccess.Read))
            {
                return await new Converter.CSV().Deserialize<Customer>(stream);
            }
        }
    }
}
