﻿using Models;
using System.Text.Json;

namespace DataParser.Component
{
    public class JsonParser : IDataParser
    {
        private readonly string _path;
        public JsonParser(string path)
        {
            _path = path;
        }
        public async Task<List<Customer>> Parse()
        {
            using (var stream = File.Open(_path, FileMode.Open, FileAccess.Read))
            {
                return await JsonSerializer.DeserializeAsync<List<Customer>>(stream);
            }
        }
    }
}
