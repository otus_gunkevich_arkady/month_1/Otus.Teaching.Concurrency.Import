﻿using Models;
using System.Xml.Serialization;

namespace DataParser.Component
{
    public class XmlParser : IDataParser
    {
        private readonly string _path;
        public XmlParser(string path)
        {
            _path = path;
        }
        public async Task<List<Customer>> Parse()
        {
            Type type = typeof(List<Customer>);
            using (var stream = File.Open(_path, FileMode.Open, FileAccess.Read))
            {
                return (List<Customer>)new XmlSerializer(type).Deserialize(stream);
            }
        }
    }
}
