﻿using Models;

namespace DataGenerator.Component
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;
        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName + ".csv";
            _dataCount = dataCount;
        }
        public async Task Generate()
        {
            List<Customer> customers = RandomCustomerGenerator.Generate(_dataCount);
            await using FileStream stream = File.Create(_fileName);
            await new Converter.CSV().Serialize(stream, customers);
        }

    }
}
