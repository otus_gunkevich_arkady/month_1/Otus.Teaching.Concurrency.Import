using DataNameGenerator;
using Models;

namespace DataGenerator.Component
{
    public static class RandomCustomerGenerator
    {
        public static List<Customer> Generate(int dataCount)
        {
            var personal = new PersonalCollection();
            var customers = new List<Customer>();
            personal.GetRandomPersonal(dataCount).ForEach(s =>
            {
                Customer newCustomers = new Customer();
                newCustomers.FullName = s.FullName;
                newCustomers.Email = s.EMail;
                newCustomers.Phone = s.Phone;
                customers.Add(newCustomers);
            });
            return customers;

        }

    }
}