﻿using System.Text.Json;

namespace DataGenerator.Component
{
    public class JsonGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;
        public JsonGenerator(string fileName, int dataCount)
        {
            _fileName = fileName + ".json";
            _dataCount = dataCount;
        }
        public async Task Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            await using FileStream createStream = File.Create(_fileName);

            //Kusok govna, polziites Newtonsoft.Json
            JsonSerializerOptions jso = new JsonSerializerOptions();
            jso.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping;

            await JsonSerializer.SerializeAsync(createStream, customers, jso);
        }
    }
}
