using Models;
using System;
using System.Xml.Serialization;

namespace DataGenerator.Component
{
    public class XmlGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;
        public XmlGenerator(string fileName, int dataCount)
        {
            _fileName = fileName + ".xml";
            _dataCount = dataCount;
        }
        public async Task Generate()
        {
            List<Customer> customers = RandomCustomerGenerator.Generate(_dataCount);
            await using FileStream stream = File.Create(_fileName);
            new XmlSerializer(customers.GetType()).Serialize(stream, customers);
        }
    }
}