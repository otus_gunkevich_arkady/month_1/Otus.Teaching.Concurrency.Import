using DataGenerator.Component;
using Models;

namespace DataGenerator
{
    public static class GeneratorFactory
    {
        //��� �� ��������� ��� � ������� �� ����������� �� ������ ���������� �����, �� � ��� ����.
        public static IDataGenerator GetGenerator(DataType typeGenerate, string fileName, int dataCount)
        {
            switch (typeGenerate)
            {
                case DataType.XML: return new XmlGenerator(fileName, dataCount);
                case DataType.JSON: return new JsonGenerator(fileName, dataCount);
                case DataType.CSV: return new CsvGenerator(fileName, dataCount);
                default: return new XmlGenerator(fileName, dataCount);
            }
        }
    }
}