namespace DataGenerator
{
    public interface IDataGenerator
    {
        Task Generate();
    }
}