﻿using Models;

namespace CustomerRepository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataBaseContext _context;
        public CustomerRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task<bool> AddCustomer(Customer customer)
        {
            try
            {
                customer.Id = 0;
                await _context.AddAsync(customer);
                await _context.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddCustomers(List<Customer> customers)
        {
            try
            {
                customers.ForEach(s => s.Id = 0);
                await _context.AddRangeAsync(customers);
                await _context.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
