﻿using Models;

namespace CustomerRepository
{
    public interface ICustomerRepository
    {
        Task<bool> AddCustomer(Customer customer);
        Task<bool> AddCustomers(List<Customer> customers);
    }
}
