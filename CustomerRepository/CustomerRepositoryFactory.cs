﻿using Models;

namespace CustomerRepository
{
    public static class CustomerRepositoryFactory
    {
        public static ICustomerRepository GetReposiotory(DataBaseContext dataBase)
        {
            return new CustomerRepository(dataBase);
        }
    }
}
