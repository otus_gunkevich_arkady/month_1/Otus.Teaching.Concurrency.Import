﻿using DataNameGenerator.Component;

namespace DataNameGenerator
{
    public class PersonalCollection
    {
        private List<Human> _listHuman;
        private Random _rnd;
        public PersonalCollection()
        {
            _rnd = new Random();
            _listHuman = new List<Human>();
            using (var reader = new StreamReader(@"Data\fio_M.txt"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(' ');
                    _listHuman.Add(new Human(true, line[0], line[1], line[2]));
                }
            }
            using (var reader = new StreamReader(@"Data\fio_W.txt"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(' ');
                    _listHuman.Add(new Human(false, line[0], line[1], line[2]));
                }
            }
        }
        public List<Personal> GetRandomPersonal(int count)
        {
            List<Personal> result = new List<Personal>();
            for (int i = 0; i < count; i++)
            {
                result.Add(RandomPersonal());
            }
            return result;
        }
        private Personal RandomPersonal()
        {
            Personal result = new Personal();
            result.FullName = RandonFullName();
            result.EMail = RandomEmail();
            result.Phone = RandomPhone();
            return result;
        }
        private string RandonFullName() =>
            _listHuman[_rnd.Next(0, _listHuman.Count)].FirstName +
                " " + _listHuman[_rnd.Next(0, _listHuman.Count)].SecondName +
                " " + _listHuman[_rnd.Next(0, _listHuman.Count)].LastName;
        private string RandomPhone() => $"+7({_rnd.Next(960, 999)}) {_rnd.Next(100,999)}-{_rnd.Next(9)}{_rnd.Next(9)}-{_rnd.Next(9)}{_rnd.Next(9)}";
        private string RandomEmail() => $"{GenerateRandomAlphabetString(_rnd.Next(5,20))}@mail.ru";
        private string GenerateRandomAlphabetString(int length)
        {
            char[] arrChar = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string result = string.Empty;
            for (int i = 0; i < length; i++)
            {
                result += arrChar[_rnd.Next(arrChar.Length)];
            }
            return result;
        }
    }
}
