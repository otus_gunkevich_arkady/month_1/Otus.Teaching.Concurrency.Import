﻿namespace DataNameGenerator.Component
{
    public class Personal
    {
        public string FullName { get; set; }
        public string EMail { get; set; }
        public string Phone { get; set; }
        public Personal() { }
    }
}
