﻿namespace DataNameGenerator.Component
{
    public class Human
    {
        /// <summary>
        /// True - мужской
        /// False - женский
        /// </summary>
        public bool Sex { get; private set; }
        public string FirstName { get; private set; }
        public string SecondName { get; private set; }
        public string LastName { get; private set; }
        public Human(bool sex, string fName, string sName, string lName) 
        {
            Sex = sex;
            FirstName = fName;
            SecondName = sName;
            LastName = lName;
        }
    }
}
