﻿namespace ConsoleMenu
{
    public class MenuVisual
    {
        private IMenuService _menuService;
        public MenuVisual(IMenuService menuService) => _menuService = menuService;
        public void Visual()
        {
            while (!_menuService.Exit())
            {
                if (!_menuService.IsBlock())
                    _menuService.Write();

                var _key = Console.ReadKey();
                if (!_menuService.IsBlock())
                {
                    switch (_key.Key)
                    {
                        case ConsoleKey.UpArrow: { _menuService.Up(); break; }
                        case ConsoleKey.DownArrow: { _menuService.Down(); break; }
                        case ConsoleKey.LeftArrow: { _menuService.Left(); break; }
                        case ConsoleKey.RightArrow: { _menuService.Right(); break; }
                        case ConsoleKey.Enter: { _menuService.Enter(); break; }
                        default: { break; }
                    }
                }
            }
        }
    }
}
