﻿using System.Diagnostics;

namespace ConsoleMenu
{
    public class MenuService : IMenuService
    {
        protected MenuCore _menu;
        protected List<MenuButton> _buttons = new List<MenuButton>();
        protected List<string> _text = new List<string>();
        private bool _isLoad = false;
        protected bool _isBlock = false;

        protected bool _exit = false;
        public virtual void Enter() => _menu.ExecuteButton();
        public virtual void Up() => _menu.UpSelect();
        public virtual void Down() => _menu.DownSelect();
        public virtual void Left() { }
        public virtual void Right() { }
        public virtual void Write()
        {
            if (!_isLoad)
            {
                _menu = new MenuCore();
                _menu.LoadButton(_buttons);
                _isLoad = true;
            }
            _menu.LoadText(_text);
            Console.Clear();
            _menu.Buttons.ForEach(s =>
            {
                if (s.IsSelect) Console.BackgroundColor = s.ColorSelect;
                Console.WriteLine($"{s.Text}");
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _menu.Text.ForEach(s => Console.WriteLine(s));
        }

        public virtual bool Exit() => _exit;
        protected virtual void Log(string text) => _text.Add(text);

        public bool IsBlock() => _isBlock;
    }

}

