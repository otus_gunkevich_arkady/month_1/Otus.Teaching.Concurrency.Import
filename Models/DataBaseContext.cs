﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Models
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DataBaseContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Filename=mini.db");//.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
    }
}
