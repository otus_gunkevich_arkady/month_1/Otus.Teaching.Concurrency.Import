﻿using ConsoleMenu;
using DataGenerator;
using Models;

namespace DotnetDev.Homework._23.Generate
{
    public class FileGeneratorMethod : MenuService
    {
        private int _count = 300000;
        private readonly (int, int) _range = (1, 2000000);
        private readonly int _step = 100000;
        private readonly string _fileName = "MethodGenerationFile";
        public FileGeneratorMethod()
        {
            _buttons = new List<MenuButton>
            {
                new MenuButton("Генерировать XML", () => Generation(DataType.XML)),
                new MenuButton("Генерировать CSV", () => Generation(DataType.CSV)),
                new MenuButton("Генерировать JSON", () => Generation(DataType.JSON)),
                new MenuButton("[*] Назад", () => _exit = true)
            };
            UpdateTextCount();
        }
        private void Generation(DataType type)
        {
            GeneratorFactory.GetGenerator(type, Path.Combine(SETTING.PATH_FILES, _fileName), _count).Generate().Wait();
            UpdateTextCount();
            _text.Add($"* Файл {_fileName}");
            _text.Add($"* Тип {type}");
            _text.Add($"* Количество {_count}");
        }
        private void UpdateTextCount()
        {
            _text = new List<string>
            {
                $"Количество строк для генерации",
                $"Управление стрелками влево/вправо (шаг {_step})",
                $"{_range.Item1} < N < {_range.Item2}",
                $"<< | {_count} | >>"
            };
        }
        public override void Left()
        {
            if (!_isBlock)
            {
                _count = Math.Clamp(_count - _step, _range.Item1, _range.Item2);
                UpdateTextCount();
            }
        }
        public override void Right()
        {
            if (!_isBlock)
            {
                if (_count == 1) _count = 0;
                _count = Math.Clamp(_count + _step, _range.Item1, _range.Item2);
                UpdateTextCount();
            }
        }
    }
}
