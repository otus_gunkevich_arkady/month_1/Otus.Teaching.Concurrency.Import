﻿using ConsoleMenu;
using DataParser;
using Models;
using System.Diagnostics;

namespace DotnetDev.Homework._23.Generate
{
    public class FileGenerator : MenuService
    {
        public FileGenerator()
        {
            _buttons = new List<MenuButton>
            {
                new MenuButton("Генерировать с программы", () => FileGenerateProgramm()),
                new MenuButton("Генерировать из метода", () => new MenuVisual(new FileGeneratorMethod()).Visual()),
                new MenuButton("[*] Назад", () => _exit = true)
            };
        }
        private void UpdateText()
        {
            _text = new List<string>
            {
                "Введите аргументы для запуска генератора файлов:",
                "========Подказка========",
                "Имя файла: -f RandomFileName",
                "Тип файла: -t [XML, CSV, JSON]",
                "Кол-во клиентов: -c 1000 (больше 0)",
                "Например: -f TreeHundredCustomer -t XML -c 300",
            };
        }
        private void FileGenerateProgramm()
        {
            UpdateText();
            Write();
            _isBlock = true;
            try
            {
                Console.Write("Параметры:");
                string args = Console.ReadLine();
                ProcessStartInfo startInfoProcess = new ProcessStartInfo
                {
                    FileName = SETTING.PATH_PROGRAMM_DATA_GENERATION_APP,
                    Arguments = args + " -p " + SETTING.PATH_FILES,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Minimized,
                };
                using (Process programm = new Process() { StartInfo = startInfoProcess })
                {
                    programm.Start();
                    _text.Add("Начало программы");
                    while (!programm.StandardOutput.EndOfStream)
                    {
                        _text.Add(programm.StandardOutput.ReadLine());
                    }
                    _text.Add("Конец программы");
                    _text.Add("Для продолжения выберите один из пунктов меню/подвигайте стрелочками");
                }
            }
            catch
            {
                _text.Add("Что то пошло не так");
            }
            _isBlock = false;
        }
    }
}
