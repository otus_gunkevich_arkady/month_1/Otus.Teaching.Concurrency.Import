﻿namespace DotnetDev.Homework._23
{
    internal static class SETTING
    {
        /// <summary>
        /// Папка куда будут складываться файлы
        /// </summary>
        public static string PATH_FILES = @"D:\Project\VS\OTUS\1-10\23\Files";
        /// <summary>
        /// Папка запуска программы
        /// </summary>
        public static string PATH_PROGRAMM_DATA_GENERATION_APP = @"D:\Project\VS\OTUS\1-10\23\DataGeneratorApp\bin\Debug\net6.0\DataGeneratorApp.exe";
    }
}
