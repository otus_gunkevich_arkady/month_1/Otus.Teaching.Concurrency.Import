﻿using ConsoleMenu;
using DotnetDev.Homework._23.Generate;
using DotnetDev.Homework._23.Load;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DotnetDev.Homework._23
{
    public class Start : MenuService
    {
        public Start()
        {
            //Это инициализация БД в начале приложения
            //Либо я не правильно понял задание
            //:)
            using (var db = new DataBaseContext())
            {
                db.Database.Migrate();
            }
            _buttons = new List<MenuButton>
                    {
                        new MenuButton("Генератор файлов.", () => new MenuVisual(new FileGenerator()).Visual()),
                        new MenuButton("Загружаем данные.", () => new MenuVisual(new FileLoad()).Visual()),
                        new MenuButton("[*] Выйти", () => _exit = true)
                    };

            _text = new List<string>
                    {
                        "",
                        "",
                        "Гункевич Аркадий Игоревич | OTUS",
                        "ДЗ: Введение в параллелизм в .NET. Отличия процесса, потока, домена и таска // ДЗ",
                        "Установленая папка загрузки файлов"
                    };
        }
    }
}
