﻿using ConsoleMenu;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DotnetDev.Homework._23
{
    public class Program
    {
        private static void Main(string[] args) => new MenuVisual(new Start()).Visual();
    }
}