﻿using ConsoleMenu;
using Models;

namespace DotnetDev.Homework._23.Load
{
    public class FileLoadSelected : MenuService
    {
        public FileLoadSelected(Action<(FileInfo, DataType)> action)
        {

            _buttons = new List<MenuButton>();

            GetFiles()
                .Where(s => s.Value != DataType.None)
                .OrderBy(s => s.Value)
                .ToList()
                .ForEach(s =>
                    { _buttons.Add(new MenuButton($"{s.Key.Name}", () => { action((s.Key, s.Value)); _exit = true; }, GetColor(s.Value))); });

            _buttons.Add(new MenuButton("[*] Назад", () => _exit = true));
        }
        private Dictionary<FileInfo, DataType> GetFiles()
        {
            return Directory.GetFiles(SETTING.PATH_FILES).Select(s => new FileInfo(s)).ToDictionary(s => s, s => GetType(s.Name));
        }

        private DataType GetType(string name) => name.Split('.').Last() switch
        {
            "xml" => DataType.XML,
            "json" => DataType.JSON,
            "csv" => DataType.CSV,
            _ => DataType.None
        };

        private ConsoleColor GetColor(DataType typeGenerate) => typeGenerate switch
        {
            DataType.XML => ConsoleColor.Magenta,
            DataType.CSV => ConsoleColor.DarkCyan,
            DataType.JSON => ConsoleColor.Blue,
            _ => ConsoleColor.Gray
        };
    }
}
