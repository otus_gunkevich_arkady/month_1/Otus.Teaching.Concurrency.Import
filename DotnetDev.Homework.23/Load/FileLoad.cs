﻿using ConsoleMenu;
using DataLoader;
using DataParser;
using Models;
using System.Diagnostics;

namespace DotnetDev.Homework._23.Load
{
    public class FileLoad : MenuService
    {
        private FileInfo _selectedFile;
        private DataType _typeFile;
        private List<Customer> _customers;
        private Stopwatch _stopwatch;
        public FileLoad()
        {
            _stopwatch = new Stopwatch();
            _buttons = new List<MenuButton>
            {
                new MenuButton("Выбрать файл", () => new MenuVisual(new FileLoadSelected(FileSelected)).Visual()),
                new MenuButton("Загрузить данные в БД", async () => await FileLoadDB()),
                new MenuButton("[*] Назад", () => _exit = true)
            };
        }
        private void FileSelected((FileInfo, DataType) selected)
        {
            _selectedFile = selected.Item1;
            _typeFile = selected.Item2;
            UpdateText();
        }
        private void UpdateText()
        {
            _text = new List<string>()
            {
                $"Выбранный файл {_selectedFile.Name}",
                $"Тип файла {_typeFile}"
            };
        }
        private async Task FileLoadDB()
        {
            this._isBlock = true;
            if (_selectedFile is null)
            {
                _text = new List<string>() { "Сначала надо выбрать файл в меню \"Выбрать файл\"" };
            }
            else
            {
                UpdateText();
                _text.Add($"Загрузка из файла {_selectedFile.FullName}");
                Write();
                _stopwatch = Stopwatch.StartNew();
                _customers = await ParserFactory.GetGenerator(_typeFile, _selectedFile.FullName).Parse();
                _stopwatch.Stop();
                _text.Add($"Количество объектов {_customers.Count()}");
                _text.Add($"Затраченное время {new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds} секунд");
                Write();
                int countThread = Process.GetCurrentProcess().Threads.Count;
                var loader = LoaderFactory.GetLoader();
                _text.Add($"Загрузка в БД");
                _text.Add($"Потоков использованно {countThread}");
                _stopwatch = Stopwatch.StartNew();
                await loader.LoadData(_customers, countThread);
                _stopwatch.Stop();
                _text.Add($"Данные загружены в БД");
                _text.Add($"Строк загружено {_customers.Count}");
                _text.Add($"Затраченное время {new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds} секунд");
                Write();
            }
            this._isBlock = false;
        }
    }
}
