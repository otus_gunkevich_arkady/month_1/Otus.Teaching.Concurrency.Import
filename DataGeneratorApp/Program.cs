﻿using DataGenerator;
using Models;

public class Program
{
    private static DataType _dataTypeGenerate;
    private static string _dataPath;
    private static string _dataFileName;
    private static int _dataCount;
    private static void Main(string[] args)
    {
        if (!TryParseArgs(args))
            return;

        GeneratorFactory.GetGenerator(_dataTypeGenerate, Path.Combine(_dataPath, _dataFileName), _dataCount).Generate().Wait();
        Console.WriteLine($"* Тип {_dataTypeGenerate}");
        Console.WriteLine($"* Файл {_dataFileName}");
        Console.WriteLine($"* Количество {_dataCount}");

    }
    private static bool TryParseArgs(string[] args)
    {
        bool result = true;
        _dataTypeGenerate = DataType.XML;
        _dataCount = 0;
        for (int i = 0; i < args.Length; i++)
        {
            if (i + 1 < args.Length)
            {
                if (args[i] == "-f")
                {
                    _dataFileName = args[i + 1];
                }
                if (args[i] == "-t")
                {
                    switch (args[i + 1].ToUpper())
                    {
                        case "XML": _dataTypeGenerate = DataType.XML; break;
                        case "CSV": _dataTypeGenerate = DataType.CSV; break;
                        case "JSON": _dataTypeGenerate = DataType.JSON; break;
                        default: _dataTypeGenerate = DataType.XML; break;
                    }
                    
                }
                if (args[i] == "-c")
                {
                    _dataCount = int.Parse(args[i + 1]);
                }
                if (args[i] == "-p")
                {
                    _dataPath = args[i + 1];
                }
                i += 1;
            }
        }
        if (_dataFileName == string.Empty || _dataFileName is null)
        {
            _dataFileName = "DotnetDev.Homework._23";
        }
        if (_dataCount < 1)
        {
            _dataCount = 1000000;
        }
        //Исскуственное ограничение. Тут можно убрать, но случайно
        //можно ввести огромное число, что повесит программу.
        if (_dataCount > 2000000) _dataCount = 2000000;
        return result;
    }
}